﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MySql.TinyDbCopy.Data
{
    public class TableSchemaData : IDisposable
    {
        private readonly MySqlConnection _connection;
        private readonly bool _includeDropIfExistsStatement;

        public TableSchemaData(string connectionString, bool includeDropIfExistsStatement)
        {
            _connection = new MySqlConnection(connectionString);
            _includeDropIfExistsStatement = includeDropIfExistsStatement;
        }

        public string ExportSchema()
        {
            var allTables = _connection.Query<string>("SHOW FULL TABLES WHERE Table_type = 'BASE TABLE';");
            var tablesWithCreate = new Dictionary<string, string>();
            var stringBuilder = new StringBuilder();

            foreach (var table in allTables)
            {
                var createScriptResult = _connection.QueryFirst($"SHOW CREATE TABLE `{table}`;");
                var createScriptCommand = (createScriptResult as IDictionary<string, object>)["Create Table"].ToString();

                tablesWithCreate.Add(table, createScriptCommand);
            }

            var tablesReArranged = ReArrangeDependencies(tablesWithCreate, "foreign key");

            foreach (var table in tablesReArranged)
            {
                var tableCreate = tablesWithCreate[table];

                stringBuilder.AppendLine($"-- `{table}`");

                if (_includeDropIfExistsStatement)
                    stringBuilder.AppendLine($"DROP TABLE IF EXISTS `{table}`;");

                stringBuilder.AppendLine($"{tableCreate};");

                stringBuilder.AppendLine();
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        private List<string> ReArrangeDependencies(IDictionary<string, string> tables, string splitKeyword)
        {
            var tablesOk = new List<string>();

            bool requireLoop = true;

            while (requireLoop)
            {
                requireLoop = false;

                foreach (var table in tables)
                {
                    if (tablesOk.Contains(table.Key))
                        continue;

                    bool allReferencedAdded = true;

                    string createSql = table.Value.ToLower();
                    string referenceInfo = "";

                    bool referenceTaken = false;
                    if (splitKeyword != null && splitKeyword != "")
                    {
                        if (createSql.Contains(string.Format(" {0} ", splitKeyword)))
                        {
                            string[] sa = createSql.Split(new string[] { string.Format(" {0} ", splitKeyword) }, StringSplitOptions.RemoveEmptyEntries);
                            referenceInfo = sa[sa.Length - 1];
                            referenceTaken = true;
                        }
                    }

                    if (!referenceTaken)
                        referenceInfo = createSql;

                    foreach (var table2 in tables.Where(x => x.Key != table.Key && !tablesOk.Contains(x.Key)))
                    {
                        if (referenceInfo.Contains(string.Format("`{0}`", table2.Key.ToLower())))
                        {
                            allReferencedAdded = false;
                            break;
                        }
                    }

                    if (allReferencedAdded)
                    {
                        if (!tablesOk.Contains(table.Key))
                        {
                            tablesOk.Add(table.Key);
                            requireLoop = true;
                            break;
                        }
                    }
                }
            }

            foreach (var kv in tables.Where(x => !tablesOk.Contains(x.Key)))
                tablesOk.Add(kv.Key);

            return tablesOk;
        }

        public void Dispose()
        {
            _connection.Dispose();
        }
    }
}