﻿using MySql.Data.MySqlClient;

namespace MySql.TinyDbCopy.Data
{
    public class CopyData
    {
        private readonly DbCopyOptions _options;
        private readonly SchemaData _schema;

        public CopyData(DbCopyOptions options)
        {
            _options = options;
            _schema = new SchemaData(options);
        }

        public void CopyDatabase()
        {
            var schemeText = _schema.ExportSchema();

            using var conn = new MySqlConnection(_options.ConnectionStringSourceDb);
            conn.Open();

            var commandCreateDb = new MySqlCommand($"CREATE DATABASE {_options.NewDbName};", conn);
            commandCreateDb.ExecuteNonQuery();

            conn.ChangeDatabase(_options.NewDbName);

            var commandCopyDb = new MySqlCommand(schemeText, conn);
            commandCopyDb.ExecuteNonQuery();
        }
    }
}