﻿using System.Text;

namespace MySql.TinyDbCopy.Data
{
    public class SchemaData
    {
        private readonly DbCopyOptions _options;

        public SchemaData(DbCopyOptions options)
        {
            _options = options;
        }

        public string ExportSchema()
        {
            string tables, views;
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;");
            stringBuilder.AppendLine("/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;");
            stringBuilder.AppendLine("/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;");
            stringBuilder.AppendLine("/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;");
            stringBuilder.AppendLine("/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;");
            stringBuilder.AppendLine("/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;");
            stringBuilder.AppendLine("/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;");
            stringBuilder.AppendLine();
            stringBuilder.AppendLine();

            if (_options.CopyTables)
            {
                using var tableSchema = new TableSchemaData(_options.ConnectionStringSourceDb, _options.IncludeDropIfExistsStatement);
                tables = tableSchema.ExportSchema();

                stringBuilder.AppendLine("-- TABLES");
                stringBuilder.AppendLine(tables);
                stringBuilder.AppendLine();
                stringBuilder.AppendLine();
            }

            if (_options.CopyViews)
            {
                using var viewSchema = new ViewSchemaData(_options.ConnectionStringSourceDb, _options.IncludeDropIfExistsStatement);
                views = viewSchema.ExportSchema();

                stringBuilder.AppendLine("-- VIEWS");
                stringBuilder.AppendLine(views);
                stringBuilder.AppendLine();
                stringBuilder.AppendLine();
            }

            stringBuilder.AppendLine("/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;");
            stringBuilder.AppendLine("/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;");
            stringBuilder.AppendLine("/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;");
            stringBuilder.AppendLine("/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;");
            stringBuilder.AppendLine("/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;");
            stringBuilder.AppendLine("/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;");
            stringBuilder.AppendLine("/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;");

            return stringBuilder.ToString();
        }
    }
}