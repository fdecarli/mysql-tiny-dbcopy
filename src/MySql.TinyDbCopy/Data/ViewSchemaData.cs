﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MySql.TinyDbCopy.Data
{
    public class ViewSchemaData : IDisposable
    {
        private readonly MySqlConnection _connection;
        private readonly bool _includeDropIfExistsStatement;

        public ViewSchemaData(string connectionString, bool includeDropIfExistsStatement)
        {
            _connection = new MySqlConnection(connectionString);
            _includeDropIfExistsStatement = includeDropIfExistsStatement;
        }

        public string ExportSchema()
        {
            var allViews = _connection.Query<string>("SHOW FULL TABLES WHERE Table_type = 'VIEW';");
            var stringBuilder = new StringBuilder();

            foreach (var view in allViews)
            {
                var createScriptResult = _connection.QueryFirst($"SHOW CREATE VIEW `{view}`;");
                var createScriptCommand = (createScriptResult as IDictionary<string, object>)["Create View"].ToString();

                stringBuilder.AppendLine($"-- `{view}`");

                var regex = new Regex("^CREATE(.+)VIEW", RegexOptions.IgnoreCase);
                createScriptCommand = regex.Replace(createScriptCommand, "CREATE VIEW ");

                if (_includeDropIfExistsStatement)
                    stringBuilder.AppendLine($"DROP VIEW IF EXISTS `{view}`;");

                stringBuilder.AppendLine($"{createScriptCommand};");

                stringBuilder.AppendLine();
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        public void Dispose()
        {
            _connection.Dispose();
        }
    }
}