﻿using MySql.TinyDbCopy.Data;
using System;

namespace MySql.TinyDbCopy
{
    public class DbCopy
    {
        public void Copy(DbCopyOptions options)
        {
            if (options == null)
                throw new ArgumentNullException($"Parameter '{nameof(options)}' is invalid.");
            else if(string.IsNullOrWhiteSpace(options.ConnectionStringSourceDb))
                throw new ArgumentException($"'{nameof(options.ConnectionStringSourceDb)}' is invalid.");
            else if (string.IsNullOrWhiteSpace(options.NewDbName))
                throw new ArgumentException($"'{nameof(options.NewDbName)}' is invalid.");

            var copyData = new CopyData(options);
            copyData.CopyDatabase();
        }
    }
}