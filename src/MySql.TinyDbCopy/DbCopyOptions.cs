﻿namespace MySql.TinyDbCopy
{
    public class DbCopyOptions
    {
        public string ConnectionStringSourceDb { get; set; }
        public string NewDbName { get; set; }
        public bool CopyTables { get; set; } = true;
        public bool CopyViews { get; set; }
        public bool IncludeDropIfExistsStatement { get; set; }
    }
}