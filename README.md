# MySqlTinyDbCopy

MySqlTinyDbCopy is a free and simple tool to clone MySql databases. 

Now, it's possible clone databases with tables and views, but soon it will be possible clone with functions, procedures, triggers, wathever.

MySqlTinyDbCopy use frameworks/libs:
- [Dapper](https://www.nuget.org/packages/Dapper/)
- [MySql.Data](https://www.nuget.org/packages/MySql.Data/)


## Download
Install via NuGet: PM> Install-Package MySqlTinyDbCopy
https://www.nuget.org/packages/MySqlTinyDbCopy

## How use
```C#
var copy = new DbCopy();
copy.Copy(new DbCopyOptions
{
    ConnectionStringSourceDb = "",
    NewDbName = "new_database",
    CopyTables = true,
    CopyViews = true,
    IncludeDropIfExistsStatement = false
});
```